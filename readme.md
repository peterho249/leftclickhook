# README #

## Information ##
* ID: 1512077
* Name: Ho Xuan Dung

## Configuration System ##
* OS: Windows 10 x64
* IDE: Visual Studio 2017

## Function ##
* Eat message of left mouse click event.
* Hook is implemented in run-time DLL.

## Application Flow ##
### Main Flow ###
* After running application, press ctrl + space to active hook. Then, you cannot use the left mouse button.
* To disable hook, press ctrl + space.

## BitBucket Link ##
* https://peterho249@bitbucket.org/peterho249/leftclickhook.git

## Youtube Link ##
* https://youtu.be/D_JmG-uBV80