// LeftClickDll.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "LeftClickDll.h"


HHOOK hHook = NULL;
HINSTANCE hinstLib;

LRESULT CALLBACK MouseHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0)
		return CallNextHookEx(hHook, nCode, wParam, lParam);

	if (wParam == WM_LBUTTONDOWN)
	{
		return 1;
	}

	return CallNextHookEx(hHook, nCode, wParam, lParam);
}
//
//LRESULT CALLBACK MouseHookProc2(int nCode, WPARAM wParam, LPARAM lParam)
//{
//	if (nCode < 0)
//		return CallNextHookEx(hHook, nCode, wParam, lParam);
//
//	SwapMouseButton(TRUE);
//	return CallNextHookEx(hHook, nCode, wParam, lParam);
//}

void _installHook(HWND hWnd)
{
	if (hHook != NULL)
		return;

	hHook = SetWindowsHookEx(WH_MOUSE_LL, (HOOKPROC)MouseHookProc, hinstLib, 0);
	if (hHook)
		MessageBox(hWnd, L"Install Hook Successfully", NULL, 0);
	else
		MessageBox(hWnd, L"Cannot Install Hook", NULL, 0);
}

void _removeHook(HWND hWnd)
{
	if (hHook == NULL)
		return;

	UnhookWindowsHookEx(hHook);
	hHook = NULL;

	MessageBox(hWnd, L"Uninstall Hook Successfully", NULL, 0);
}